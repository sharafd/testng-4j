package ru.sharaf.testng4j.testng;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 *  TestNG test sample
 *  Родительский класс, работает с тестовым окружением.
 */

@Test
public class AppTestNGTest {
	
	 public Path tempDir;
	 
	 /**
	  * Подготовка тестовых данных
	  * Создание временного каталога
	  * @throws IOException
	  */
	 @BeforeClass(alwaysRun = true)
	 @Parameters("prefix")
	 public void makeTempDir(@Optional String Prefix) throws IOException{
		
		 System.out.println("Prepare test envirovment...");
   
		 tempDir = Files.createTempDirectory(Prefix); 
		 
		 System.out.println("temp dir: " + tempDir.toString());	 		
	  }	  
	
	  /**
	     * Rigorous Test :-)
	  */
	  @Test(enabled = false)
	  public void test() throws Exception {
		  
		System.out.println("Rigorous Test");  
	    AssertJUnit.assertTrue(true);
	  }	 
	  
	  @Test(groups = "negative, broken", priority = 0)
	  // не должен запускаться, т.к. broken 
	  public void brokenPositiveTest() throws IOException {
		 
		  System.out.println("Negative test: broken");	
	  }   	
	  /**
	   * 
	   * Удаляем временный каталог
	   * @throws IOException
	   */
	  @AfterClass(alwaysRun = true)
	  public void deleteTempDir() throws IOException{
	    
		// SoftAssert s = new SoftAssert();  
		 System.out.println("Cleaning test envirovment...");
		 // Есть, что удалять?
		// s.assertTrue(Files.exists(tempDir));
          FileUtils.deleteDirectory(tempDir.toFile());
         // Каталог удалён?
    	// s.assertFalse(Files.exists(tempDir));
        // s.assertAll();
         System.out.println("temp dir " + tempDir.toString() + " deleted.");
	  }	  
}