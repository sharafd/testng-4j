package ru.sharaf.testng4j.testng;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import ru.sharaf.testng4j.App;
import ru.sharaf.testng4j.common.DataProviders;



/**
 * TestNG DataProvidered tests sample
 *
 */
public class TestNGDataProvideredTest extends AppTestNGTest{
	
	  /**
	   * Пытаемся создать файл в тестовом каталоге из @BeforeMethod
	   * Имена файлов берем из DataProvider
	   * @throws IOException
	   */
	  @Test(groups = "parameterized", dataProviderClass = DataProviders.class, dataProvider="filename")
	  public void createRandomFilenameFileTest(String fname) throws IOException {  

		System.out.println("Parameterized Test 1");

		assertTrue(App.createFile(tempDir.toString(), fname));
	  }
	  
	  /**
	   * Пытаемся создать файл в тестовом каталоге из @BeforeMethod
	   * Имена файлов берем из DataProvider
	   * @throws IOException
	   */
	  @Test(groups = "parameterized", dataProviderClass = DataProviders.class, dataProvider="loadFromFile")
	  public void createFileTest(String name, String extension) throws IOException {  

		System.out.println("Parameterized Test 2");

		assertTrue(App.createFile(tempDir.toString(), name + "." + extension));
	  }
	  
	  
	  
	  
	   @Test(alwaysRun = true) // не выполняется
	//  @Test(groups = "parameterized") // а так все ОК
	   public void printSlashes() {	  
		  System.out.println(" ---- parameterized test ----");
	  }

}
