package ru.sharaf.testng4j.testng;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import java.nio.file.Files;
import org.testng.annotations.*;

import ru.sharaf.testng4j.App;
import ru.sharaf.testng4j._3dparty.SoftHamcrestAssert;

import static org.hamcrest.CoreMatchers.*;

/**
 * TestNG parameterized tests sample
 *
 */

public class TestNGCreateNewFileTest extends AppTestNGTest{

	  private int Stage = 1;
	  
	  @BeforeMethod 
	  // Есть ли папка, куда будем складывать файлы
	  public void isTempDirExist() {
		  
		  System.out.println("Stage: " + Stage + " --------");
		  
		  assertTrue(Files.exists(tempDir));
		 
		  System.out.println("Directory " + tempDir.toString() + " found."); 
		  
		  Stage++;
	  }
	  
	  @AfterMethod 
	  public void printSlashes() {
		  
		  System.out.println(" ---- ----");
	  }
	  
	  @AfterGroups(groups = "negative") 
	  public void printSlashes2() {
		  
		  System.out.println(" ----  negative tests done ----");
	  }
	  	  
	  /**
	   * Пытаемся создать файл в тестовом каталоге из @BeforeMethod
	   * @throws IOException
	   */
	  @Test(groups = "positive", priority = 1)
	  public void createFileTest() throws IOException {  

		System.out.println("Positive test 1:");

		assertTrue(App.createFile(tempDir.toString(), "filename"));
	  }
	  
	  /**
	   * Пустое имя файла
	   * @throws IOException
	   */
	  @Test(groups = "negative")
	  public void createNullFileNameTest() throws IOException {
		  
		System.out.println("Negative test 1:");
		  
		assertFalse(App.createFile(tempDir.toString(), ""));  	
		System.out.println("NULL filename not granted");	
	  }	
	  
	  /**
	   *  Если искомый файл есть - это хорошо 
	   */
	  @Test(groups = "positive", priority = 2)
	  public void isFileExistTest() {
		  
		 System.out.println("Positive test 2:");

		 assertTrue(App.filename.exists());
		 System.out.println(App.filename.toString() + " found."); 
	  }
	  
	  /**
	   * В /root ничего писать нельзя
	   * @throws IOException
	   */
	  @Test(groups = "negative")
	  public void createFileNotGrantedTest() throws IOException {
		  
		System.out.println("Negative test 2:");
		
		assertFalse(App.createFile("/root", "empty"));  	  
	    System.out.println("create file not granted");
	  }	
	  
	  /**
	   *  Не передали имя папки
	   * @throws IOException
	   */
	  @Test(groups = "negative")
	  public void nullDirectoryTest() throws IOException {
		  
		System.out.println("Negative test 3:");
		
		assertFalse(App.createFile("", "empty")); 	
    	System.out.println("NULL directory given");
	  }
	  
	  /**
	   * Попробуем что-нибудь записать в созданный файл
	   * @throws IOException
	   */
	  @Test(groups = "positive", dependsOnMethods = "createFileTest" )
	  public void canWriteInFileTest () throws IOException {
		 
		  SoftHamcrestAssert h = new SoftHamcrestAssert();
		    
		  System.out.println("Positive test 3:");
		  h.assertThat("File exist",App.filename.exists(), is(true));
		  
		  PrintWriter writer = new PrintWriter(App.filename, "UTF-8");
	      writer.println("1234567890");
	      writer.close();
		      
	      BufferedReader br = new BufferedReader(new FileReader(App.filename));
	      String line;
	      
	      while ((line = br.readLine()) != null) {
	    	  
	    	  // прочитали то, что записали?
	    	  h.assertThat("Line matched",line, is("1234567890"));
	    	  System.out.println(App.filename.toString() + " content: " + line);
	      }
	      br.close();
	      
	      h.assertAll();
	  }   	  
	  
	  @Test(groups = "positive, broken", priority = 0)
	  // не должен запускаться
	  public void brokenPositiveTest() throws IOException {
		 
		  System.out.println("Positive test 4: broken");
		
	  }   	  
}
