package ru.sharaf.testng4j.testng;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import ru.sharaf.testng4j.App;
import ru.sharaf.testng4j.common.DataProviders;

public class TestNGParallelRunnableTest extends AppTestNGTest{
	
/**
 * TestNG DataProvidered Parallel Runnable tests sample
 *
 */
	
	  /**
	   * Пытаемся создать файл в тестовом каталоге из @BeforeMethod
	   * Имена файлов берем из DataProvider
	   * @throws IOException
	 * @throws InterruptedException 
	   */
	  @Test(dataProviderClass = DataProviders.class, dataProvider="filename")
	  public void createRandomFilenameFileTest(String fname) throws IOException, InterruptedException {  

		System.out.println("Parallel runnable Test 1");

		assertTrue(App.createFile(tempDir.toString(), fname));
		
		Thread.sleep(1000);
	  }
	  
	  @Test(dataProviderClass = DataProviders.class, dataProvider="filename")
	  public void createRandomFilenameFileTest2(String fname) throws IOException, InterruptedException {  

	    System.out.println("Parallel runnable Test 2");

		assertTrue(App.createFile(tempDir.toString(), fname));
		
		Thread.sleep(1000);
	  }
	  

}

