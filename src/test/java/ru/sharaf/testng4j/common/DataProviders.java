package ru.sharaf.testng4j.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.testng.annotations.DataProvider;

/**
 * TestNG parameterized tests Data Provider example
 * @author Damir
 *
 */

public class DataProviders {

	/**
	 * Имена файлов
	 * @return
	 */
	 @DataProvider
	  public static Iterator<Object> filename() {
		    List<Object> data = new ArrayList<Object>();
		    for (int i = 0; i < 5; i++) {
		      data.add(new Object[]{
		    		 generateRandomFilename()
		      });
		    }
		    return data.iterator();
	  }
	 
	 /**
	 * 
	 * Based on TestNG Samples by Barancev
	 * https://github.com/barancev/testng_samples.git
	 * Загрузка данных из внешнего файла
	 */
	  @DataProvider
	  public static Iterator<Object[]> loadFromFile() throws IOException {
	     BufferedReader in = new BufferedReader(new InputStreamReader(
	         DataProviders.class.getResourceAsStream("/files.dat")));
	     
	     List<Object[]> userData = new ArrayList<Object[]>();
	     String line = in.readLine();
	     while (line != null) {
	       userData.add(line.split(";"));
	       line = in.readLine();
	     }
	     
	     in.close();
	     
	     return userData.iterator();
	   }
	 
	 /**
	  * Генератор случайных имён файлов 
	  * @return
	  */
	  private static String generateRandomFilename() {
		  
	    return String.valueOf(new Random().nextInt());
	  } 
}
