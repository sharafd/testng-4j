package ru.sharaf.testng4j._3dparty;

/**
* TestNG Samples by Barancev
* https://github.com/barancev/testng_samples.git  
*/

import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;

public class SoftHamcrestAssert extends SoftAssert {

  public <T> void assertThat(final T actual, final Matcher<? super T> matcher) {
    doAssert(new IAssert() {
      @Override
      public void doAssert() {
        MatcherAssert.assertThat(actual, matcher);
      }

      @Override
      public Object getActual() {
        return actual;
      }

      @Override
      public Object getExpected() {
        return null;
      }

      @Override
      public String getMessage() {
        return null;
      }
    });
  }

  public <T> void assertThat(final String reason, final T actual, final Matcher<? super T> matcher) {
    doAssert(new IAssert() {
      @Override
      public void doAssert() {
        MatcherAssert.assertThat(reason, actual, matcher);
      }

      @Override
      public Object getActual() {
        return actual;
      }

      @Override
      public Object getExpected() {
        return null;
      }

      @Override
      public String getMessage() {
        return reason;
      }
    });
  }

}
