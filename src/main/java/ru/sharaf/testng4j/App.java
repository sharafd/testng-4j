package ru.sharaf.testng4j;

import java.io.File;
import java.io.IOException;

/**
 * Simple Application
 *
 */
public class App 
{
    public static File filename;
	
	public static void main( String[] args ) {
        
    	boolean res;
    	
    	if (args.length == 0) {
    	
    		System.out.println( "commands: " );
     		System.out.println( "cf - create file 'emptyFile' in [path] directory." );
 
    	} else {
    		// make file "emptyFile"
            if (args[0].equals("cf"))  {
    			
    		    res = createFile(args[1],"emptyFile");
        
    			if (res){
            
    				System.out.println( "emptyFile created in directory " + args[1]);
        	
    			} else {
            	
    				System.out.println( "Failed to create emptyFile in directory " + args[1]);
            	}
    		}
        }
    }    
  
   // Создаём пустой файл по указанному пути 
   public static boolean createFile(String dir, String fname) {
    	
        boolean res = false;
    	
    	System.out.println("attempt to create file '" + fname + "' in " + dir + "...");
    	
    	filename =  new File(dir + "/" + fname);
    		
    	try {
			res = filename.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	    	
        return res;
    } 

}


